<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    protected $fillable = [
      'class_code',
      'name',
      'description',
      'room',
      'teacher_id'
    ];

    //protected $touches = ['teacher', 'student'];

    public function teacher(){
      return $this->belongsTo(Teacher::class);
    }

    public function students(){
      return $this->belongsToMany(Student::class, 'classrooms_students');
    }
}
