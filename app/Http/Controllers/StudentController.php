<?php

namespace App\Http\Controllers;

use App\Student;
use App\Classroom;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    public function __construct(){
      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view', Student::class);

        $students = Student::orderBy('nric', 'asc')->get();

        return view('students.index', [
            'students' => $students
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Student::class);

        $student = new Student();

        return view('students.create', [
            'student' => $student,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'student_id' => [
            'required',
            'unique:students',
            'regex:/^([1-9]{2})([0-9]{3})$/'
          ],
          'nric' => [
            'required',
            'regex:/^([0-9]{2})([0-1]{1})([0-9]{1})([0-3]{1})([0-9]{1})([0-9]{6})$/'
          ],
          'name' => 'required|max:100',
          'gender' => 'required',
          'address' => 'required|max:500',
          'postcode' => [
            'required',
            'regex:/^([0-9]{5})$/'
          ],
          'city' => 'required|max:50',
          'state' => 'required',
          'guardian' => 'required|max:100',
          'guardian_phone_no' => [
            'required',
            'regex:/^([0-9]{10})$/'
          ]
        ]);

        $student = new Student;
        $student->fill($request->except('classroom_id'));
        $student->save();
        $student->fill($request->all());
        $student->classrooms()->attach($student->classroom_id);

        return redirect()->route('student.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::find($id);
        if(!$student) throw new ModelNotFoundException;

        $this->authorize('view', $student);

        return view('students.show', [
            'student' => $student
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);
        if(!$student) throw new ModelNotFoundException;

        $this->authorize('manage', $student);

        return view('students.edit', [
          'student' => $student
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
          'student_id' => [
            'required',
            'regex:/^([1-9]{2})([0-9]{3})$/'
          ],
          'nric' => [
            'required',
            'regex:/^([0-9]{2})([0-1]{1})([0-9]{1})([0-3]{1})([0-9]{1})([0-9]{6})$/'
          ],
          'name' => 'required|max:100',
          'gender' => 'required',
          'address' => 'required|max:500',
          'postcode' => [
            'required',
            'regex:/^([0-9]{5})$/'
          ],
          'city' => 'required|max:50',
          'state' => 'required',
          'guardian' => 'required|max:100',
          'guardian_phone_no' => [
            'required',
            'regex:/^([0-9]{10})$/'
          ]
        ]);

        $student = Student::find($id);
        if(!$student) throw new ModelNotFoundException;

        $this->authorize('manage', $student);

        $student->fill($request->except('classroom_id'));
        $student->save();
        $student->fill($request->all());
        $student->classrooms()->sync($student->classroom_id);
        return redirect()->route('student.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
