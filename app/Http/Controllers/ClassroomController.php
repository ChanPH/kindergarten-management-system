<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\Student;
use Illuminate\Http\Request;

class ClassroomController extends Controller
{
    public function __construct(){
      $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view', Classroom::class);

        $classroom = Classroom::orderBy('class_code', 'asc')->get();

        return view('classrooms.index', [
            'classroom' => $classroom
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $this->authorize('create', Classroom::class);

      $classroom = new Classroom();

      return view('classrooms.create', [
          'classroom' => $classroom,
      ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'class_code' => [
          'required',
          'unique:classrooms',
          'regex:/^([0-9,A-Z]{4})$/'
        ],
        'name' => 'required|max:50',
        'description' => 'required|max:150',
        'room' => 'required',
        'teacher_id' => 'required'
      ]);

        $class = new Classroom();
        $class->fill($request->all());
        $class->save();

        return redirect()->route('classroom.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $classroom = Classroom::find($id);
        if(!$classroom)throw new ModelNotFoundException;

        $this->authorize('view', $classroom);

        return view('classrooms.show', [
            'classroom' => $classroom
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classroom = Classroom::find($id);
        if(!$classroom) throw new ModelNotFoundException;

        $this->authorize('manage', $classroom);

        return view('classrooms.edit', [
            'classroom' => $classroom
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'class_code' => [
          'required',
          'regex:/^([0-9,A-Z]{4})$/'
        ],
        'name' => 'required|max:50',
        'description' => 'required|max:150',
        'room' => 'required',
        'teacher_id' => 'required'
      ]);

        $classroom = Classroom::find($id);
        if(!$classroom) throw new ModelNotFoundException;

        $this->authorize('manage', $classroom);

        $classroom->fill($request->all());
        $classroom->save();
        return redirect()->route('classroom.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
