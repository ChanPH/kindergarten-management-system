<?php

namespace App\Http\Controllers;

use App\Teacher;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    public function __construct(){
      $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view', Teacher::class);

        $teachers = Teacher::orderBy('name', 'asc')->get();

        return view('teachers.index', [
            'teachers' => $teachers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Teacher::class);

        $teacher = new Teacher();

        return view('teachers.create', [
            'teacher' => $teacher
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'teacher_id' => [
            'required',
            'unique:teachers',
            'regex:/^([A-Z]{1})([0-9]{3})$/'
          ],
          'nric'=>[
            'required',
            'regex:/^([0-9]{2})([0-1]{1})([0-9]{1})([0-3]{1})([0-9]{1})([0-9]{6})$/',
          ],
          'name'=> 'required|max:100',
          'gender' => 'required',
          'phone_no' => [
            'required',
            'regex:/^([0-9]{10})$/'
          ],
          'qualification'=>'required'
        ]);

        $teacher = new Teacher;
        $teacher->fill($request->all());
        $teacher->save();

        return redirect()->route('teacher.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $teacher = Teacher::find($id);
        if(!$teacher) throw new ModelNotFoundException;

        $this->authorize('view', $teacher);

        return view('teachers.show', [
            'teacher'=>$teacher
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher=Teacher::find($id);
        if(!$teacher)throw new ModelNotFoundException;

        $this->authorize('manage', $teacher);

        return view('teachers.edit', [
            'teacher' => $teacher
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'teacher_id' => [
          'required',
          'regex:/^([A-Z]{1})([0-9]{3})$/'
        ],
        'nric'=>[
          'required',
          'regex:/^([0-9]{2})([0-1]{1})([0-9]{1})([0-3]{1})([0-9]{1})([0-9]{6})$/',
        ],
        'name'=> 'required|max:100',
        'gender' => 'required',
        'phone_no' => [
          'required',
          'regex:/^([0-9]{10})$/'
        ],
        'qualification'=>'required'
      ]);

        $teacher = Teacher::find($id);
        if(!$teacher)throw new ModelNotFoundException;

        $this->authorize('manage', $teacher);

        $teacher->fill($request->all());
        $teacher->save();
        return redirect()->route('teacher.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
