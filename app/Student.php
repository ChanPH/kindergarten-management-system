<?php

namespace App;

use App\Student;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
      'student_id',
      'nric',
      'name',
      'gender',
      'address',
      'postcode',
      'city',
      'state',
      'guardian',
      'guardian_phone_no',
      'classroom_id'
    ];

    public function classrooms(){
      return $this->belongsToMany('App\Classroom', 'classrooms_students')->withPivot('classroom_id');
    }

}
