<?php
  namespace App;

class Common{
  public static $states = [
      '01' => 'Johor',
      '02' => 'Kedah',
      '03' => 'Kelantan',
      '04' => 'Melaka',
      '05' => 'Negeri Sembilan',
      '06' => 'Pahang',
      '07' => 'Penang',
      '08' => 'Perak',
      '09' => 'Perlis',
      '10' => 'Selangor',
      '11' => 'Terengganu',
      '12' => 'Sabah',
      '13' => 'Sarawak',
      '14' => 'Kuala Lumpur',
      '15' => 'Labuan',
  ];

  public static $genders = [
      'M' => 'Male',
      'F' => 'Female',
  ];

  public static $rooms = [
      '101' => 'Room 101 (can occupy 50 students)',
      '102' => 'Room 102 (can occupy 70 students)',
      '103' => 'Room 103 (can occupy 20 students)',
      '104' => 'Room 104 (can occupy 50 students)',
      '105' => 'Room 105 (can occupy 20 students)',
      '106' => 'Room 106 (can occupy 25 students)',
      '107' => 'Room 107 (can occupy 70 students)',
      '108' => 'Room 108 (can occupy 20 students)',
      '109' => 'Room 109 (can occupy 50 students)',
  ];

  public static $qualifications = [
      'AdvDip' => 'Advance Diploma',
      'AssocDeg' => 'Associate Degree',
      'B' => 'Bachelor Degree',
      'B.(Hons)' => 'Bachelor Honours Degree',
      'GradCert' => 'Graduate Certificate',
      'GradDip' => 'Graduate Diploma',
      'Cert' => 'Gertificate IV',
      'D' => 'Doctoral Degree',
      'Dip' => 'Diploma',
      'M.(Coursework)' => 'Masters Degree (Coursework)',
      'M.(Research)' => 'Master Degree (Research)',
      'M*' => 'Master Degree (Extended)',
  ];
}

?>
