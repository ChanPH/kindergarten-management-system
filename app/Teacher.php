<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = [
      'teacher_id',
      'nric',
      'name',
      'gender',
      'phone_no',
      'qualification',
      'class_id'
    ];

    public function classrooms(){
      return $this->hasMany(Classroom::class);
    }
}
