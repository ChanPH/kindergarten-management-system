<?php

namespace App\Policies;

use App\User;
use App\Student;
use Bouncer;
use Illuminate\Auth\Access\HandlesAuthorization;

class StudentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view(User $user){
      return $user->can('view-student');
    }

    public function create(User $user){
      return $user->can('create-student');
    }

    public function manage(User $user, Student $student){
      return $user->can('manage-student');
    }
}
