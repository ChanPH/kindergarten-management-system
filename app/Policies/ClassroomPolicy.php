<?php

namespace App\Policies;

use App\User;
use App\Classroom;
use Bouncer;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClassroomPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function view(User $user){
      return $user->can('view-classroom');
    }

    public function create(User $user){
      return $user->can('create-classroom');
    }

    public function manage(User $user, Classroom $classroom){
      return $user->can('manage-classroom');
    }
}
