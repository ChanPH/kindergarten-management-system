<?php

namespace App\Policies;

use App\User;
use App\Teacher;
use Bouncer;
use Illuminate\Auth\Access\HandlesAuthorization;

class TeacherPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view(User $user){
      return $user->can('view-teacher');
    }

    public function create(User $user){
      return $user->can('create-teacher');
    }

    public function manage(User $user, Teacher $teacher){
      return $user->can('manage-teacher');
    }
}
