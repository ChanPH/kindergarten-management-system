<?php

namespace App\Providers;

use App\Policies\TeacherPolicy;
use App\Policies\StudentPolicy;
use App\Policies\ClassroomPolicy;
use App\Teacher;
use App\Student;
use App\Classroom;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Teacher::class=> TeacherPolicy::class,
        Student::class=> StudentPolicy::class,
        Classroom::class=> ClassroomPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
