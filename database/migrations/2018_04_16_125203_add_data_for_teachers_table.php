<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataForTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teachers', function (Blueprint $table) {
            //
        });

        $data = array(
          array(
            'teacher_id' => 'T001',
            'nric' => '880314145894',
            'name' => 'Lim Ai Xuan',
            'gender' => 'F',
            'phone_no' => '0123849558',
            'qualification' => 'AdvDip',
            'created_at' => '2018-04-21 09:58:02',
            'updated_at' => '2018-04-21 09:58:02'
          ),
          array(
            'teacher_id' => 'T002',
            'nric' => '870315689723'
            'name' => 'Lim Zhe Shen',
            'gender' => 'M',
            'phone_no' => '0128675889',
            'qualification' => 'AdvDip',
            'created_at' => '2018-04-25 10:00:01',
            'updated_at' => '2018-04-25 10:00:01'
          ),
          array(
            'teacher_id' => 'T003',
            'nric' => '890213145869'
            'name' => 'Hong Kee Kee',
            'gender' => 'M',
            'phone_no' => '0123849558',
            'qualification' => 'AdvDip',
            'created_at' => '2018-04-26 11:00:05',
            'updated_at' => '2018-04-26 11:00:05'
          ),
          array(
            'teacher_id' => 'T004',
            'nric' => '870313145829'
            'name' => 'Chan Kang Ren',
            'gender' => 'M',
            'phone_no' => '0112957489',
            'qualification' => 'AdvDip',
            'created_at' => '2018-04-27 12:00:05',
            'updated_at' => '2018-04-27 12:00:05'
          ),
          array(
            'teacher_id' => 'T005',
            'nric' => '910213151678'
            'name' => 'Tan Kuan Ying',
            'gender' => 'F',
            'phone_no' => '0128940338',
            'qualification' => 'AdvDip',
            'created_at' => '2018-04-28 12:12:05',
            'updated_at' => '2018-04-28 12:12:05'
          )
        );
        DB::table('teachers')->insert($data);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teachers', function (Blueprint $table) {
            //
        });
    }
}
