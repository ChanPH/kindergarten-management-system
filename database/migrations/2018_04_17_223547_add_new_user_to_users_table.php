<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewUserToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::table('users')->insert([
        'email' => 'teacher@teacher.com',
        'password' => bcrypt('rehceat'),
        'name' => 'Teacher'
      ]);

      DB::table('users')->insert([
        'email' => 'student@student.com',
        'password' => bcrypt('tenduts'),
        'name' => 'Student'
      ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
