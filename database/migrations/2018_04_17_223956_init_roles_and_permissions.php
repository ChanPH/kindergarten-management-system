<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class InitRolesAndPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $admin = Bouncer::role()->create([
          'name' => 'admin',
          'title' => 'Administrator',
        ]);

        $teacher = Bouncer::role()->create([
          'name' => 'teacher',
          'title' => 'Teacher',
        ]);

        $student = Bouncer::role()->create([
          'name' => 'student',
          'title' => 'Student'
        ]);

        $viewStudent = Bouncer::ability()->create([
          'name' => 'view-student',
          'title' => 'View Student'
        ]);

        $createStudent = Bouncer::ability()->create([
          'name' => 'create-student',
          'title' => 'Create Student'
        ]);

        $manageStudent = Bouncer::ability()->create([
          'name' => 'manage-student',
          'title' => 'Manage Student'
        ]);

        $viewTeacher = Bouncer::ability()->create([
          'name' => 'view-teacher',
          'title' => 'View Teacher'
        ]);

        $createTeacher = Bouncer::ability()->create([
          'name' => 'create-teacher',
          'title'=> 'Create Teacher'
        ]);

        $manageTeacher = Bouncer::ability()->create([
          'name' => 'manage-teacher',
          'title' => 'Manage Teacher'
        ]);

        $viewClassroom = Bouncer::ability()->create([
          'name' => 'view-classroom',
          'title' => 'View Classroom'
        ]);

        $createClassroom = Bouncer::ability()->create([
          'name' => 'create-classroom',
          'title' => 'Create Classroom'
        ]);

        $manageClassroom = Bouncer::ability()->create([
          'name' => 'manage-classroom',
          'title' => 'Manage Classroom'
        ]);

        // Assign abilities to roles
        Bouncer::allow($student)->to($viewStudent);
        Bouncer::allow($student)->to($viewClassroom);
        Bouncer::allow($student)->to($viewTeacher);

        Bouncer::allow($teacher)->to($viewStudent);
        Bouncer::allow($teacher)->to($manageStudent);
        Bouncer::allow($teacher)->to($viewTeacher);
        Bouncer::allow($teacher)->to($viewClassroom);

        Bouncer::allow($admin)->to($viewStudent);
        Bouncer::allow($admin)->to($viewTeacher);
        Bouncer::allow($admin)->to($viewClassroom);
        Bouncer::allow($admin)->to($createTeacher);
        Bouncer::allow($admin)->to($createStudent);
        Bouncer::allow($admin)->to($createClassroom);
        Bouncer::allow($admin)->to($manageStudent);
        Bouncer::allow($admin)->to($manageTeacher);
        Bouncer::allow($admin)->to($manageClassroom);

        //Make the first user an administrator
        $user = User::find(1);
        Bouncer::assign('admin')->to($user);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
