<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataForStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
          //
        });

        $data = array(
          array(
            'student_id' => '18001',
            'nric' => '990313144589',
            'name' => 'Tan Yong Peng',
            'gender' => 'M',
            'address' => '23, Jalan Jelutung, 56900 Kuala Lumpur',
            'postcode' => '56900',
            'city' => 'Kuala Lumpur',
            'state' => '14',
            'guardian' => 'Tan Yong Hong',
            'guardian_phone_no' => '0129483998',
            'created_at' => '2018-04-21 09:58:02',
            'updated_at' => '2018-04-21 09:58:02'
          ),
          array(
            'student_id' => '18002',
            'nric' => '990212145678',
            'name' => 'Lim Zhi Xin',
            'gender' => 'F',
            'address' => '36, Jalan Jelutung, 54300 Kuala Lumpur',
            'postcode' => '54300',
            'city' => 'Kuala Lumpur',
            'state' => '14',
            'guardian' => 'Lim Yong Zheng',
            'guardian_phone_no' => '0112934586',
            'created_at' => '2018-04-25 10:00:01',
            'updated_at' => '2018-04-25 10:00:01'
          ),
          array(
            'student_id' => '18003',
            'nric' => '981222148859',
            'name' => 'Joseph Wong Jing Kang',
            'gender' => 'M',
            'address' => '5/34, Jalan Impian, 56700 Kuala Lumpur',
            'postcode' => '56700',
            'city' => 'Kuala Lumpur',
            'state' => '14',
            'guardian' => 'Frank Wong Long Sheng',
            'guardian_phone_no' => '0169540339',
            'created_at' => '2018-04-26 11:00:05',
            'updated_at' => '2018-04-26 11:00:05'
          ),
          array(
            'student_id' => '18004',
            'nric' => '990316148983',
            'name' => 'Sabrina Chan Mei Mei',
            'gender' => 'F',
            'address' => '6/88a, Jalan Sehaluan, 58900 Kuala Lumpur',
            'postcode' => '58900',
            'city' => 'Kuala Lumpur',
            'state' => '14',
            'guardian' => 'Howard Chan Hong Kuan',
            'guardian_phone_no' => '0189893887',
            'created_at' => '2018-04-27 12:00:05',
            'updated_at' => '2018-04-27 12:00:05'
          ),
          array(
            'student_id' => '18005',
            'nric' => '990422145948',
            'name' => 'Tong Mei Xian',
            'gender' => 'F',
            'address' => '8/67a, Jalan Kebetulan, 58900 Kuala Lumpur',
            'postcode' => '58900',
            'city' => 'Kuala Lumpur',
            'state' => '14',
            'guardian' => 'Tong Kai Xin',
            'guardian_phone_no' => '0113943983',
            'created_at' => '2018-04-28 12:12:05',
            'updated_at' => '2018-04-28 12:12:05'
          )
        );
        DB::table('students')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            //
        });
    }
}
