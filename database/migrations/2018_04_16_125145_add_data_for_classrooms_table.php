<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataForClassroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('classrooms', function (Blueprint $table) {
            //
        });

        $data = array(
          array(
            'class_code' => 'MATH1',
            'name' => 'MATHEMATICS',
            'description' => 'Mathematics for beginner',
            'room' => '106',
            'teacher_id' => '1',
            'created_at' => '2018-04-21 09:58:02',
            'updated_at' => '2018-04-21 09:58:02'
          ),
          array(
            'class_code' => 'BI001',
            'name' => 'BAHASA INGEERIS',
            'description' => 'English for beginner',
            'room' => '102',
            'teacher_id' => '2',
            'created_at' => '2018-04-25 10:00:01',
            'updated_at' => '2018-04-25 10:00:01'
          ),
          array(
            'class_code' => 'BM001',
            'name' => 'Bahasa MELAYU',
            'description' => 'Malay for beginner',
            'room' => '105',
            'teacher_id' => '3',
            'created_at' => '2018-04-26 11:00:05',
            'updated_at' => '2018-04-26 11:00:05'
          ),
          array(
            'class_code' => 'SCI01',
            'name' => 'SCIENCE',
            'description' => 'Science for beginner',
            'room' => '107',
            'teacher_id' => '4',
            'created_at' => '2018-04-27 12:00:05',
            'updated_at' => '2018-04-27 12:00:05'
          ),
          array(
            'class_code' => 'BC001',
            'name' => 'Bahasa Cina',
            'description' => 'Chinese for beginner',
            'room' => '101',
            'teacher_id' => '5',
            'created_at' => '2018-04-28 12:12:05',
            'updated_at' => '2018-04-28 12:12:05'
          )
        );
        DB::table('classrooms')->insert($data);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('classrooms', function (Blueprint $table) {
            //
        });
    }
}
