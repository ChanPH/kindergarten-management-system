<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('student_id', 5)->unique();
            $table->string('nric', 12)->index();
            $table->string('name', 100)->index();
            $table->char('gender', 1)->index();
            $table->string('address')->nullable();
            $table->string('postcode', 5)->index();
            $table->char('state', 2)->index();
            $table->string('guardian', 100)->index();
            $table->string('guardian_phone_no', 10)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
