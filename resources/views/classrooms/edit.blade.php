<?php
  use App\Common;
  use App\Teacher;
  use App\Classroom;
?>
@extends('layouts.app')
@section('content')

@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

  <!-- Bootstrap Boilerplate -->
    <!-- Existing Classroom Form -->
    {!! Form::model($classroom, [
        'route' => [ 'classroom.update', $classroom->id ],
        'method' => 'put',
        'class' => 'form-horizontal'
    ]) !!}
      <!-- Classroom Code -->
      <div class="form-group row">
        {!! Form::label('classroom_code', 'Classroom Code', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('class_code', $classroom->class_code, [
              'id' => 'class_code',
              'class' => 'form-control',
              'maxlength' => 4
          ]) !!}
        </div>
      </div>

      <!-- name -->
      <div class="form-group row">
        {!! Form::label('classroom_name', 'Classroom Name', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('name', $classroom->name, [
              'id' => 'name',
              'class' => 'form-control',
              'maxlength' => 50
          ]) !!}
        </div>
      </div>

      <!-- description -->
      <div class="form-group row">
        {!! Form::label('classroom_description', 'Classroom Description', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::textarea('description', $classroom->description, [
              'id' => 'description',
              'class' => 'form-control'
          ]) !!}
        </div>
      </div>

      <!-- room -->
      <div class="form-group row">
        {!! Form::label('classroom_room', 'Room', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::select('room', Common::$rooms, $classroom->room, [
              'class' => 'form-control',
              'placeholder' => '- Select room -'
          ]) !!}
        </div>
      </div>

      <!-- Class Teacher -->
      <div class="form-group row">
        {!! Form::label('class_teacher_id', 'Teacher In Charge', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::select('teacher_id',
              Teacher::pluck('name', 'id'), null, [
                'class' => 'form-control',
                'placeholder' => '- Select Teacher -'
          ]) !!}
        </div>
      </div>

      <!-- Submit -->
      <div class="form-group row">
        <div class="col-sm-offset-3 col-sm-6">
          {!! Form::button('Update', [
              'type' => 'submit',
              'class' => 'btn btn-primary'
          ]) !!}
        </div>
      </div>

    {!! Form::close() !!}
  </div>

@endsection
