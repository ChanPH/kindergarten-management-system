<?php
  use App\Common;
  use App\Teacher;
?>
@extends('layouts.app')
@section('content')

@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

  <!-- Bootstrap Boilerplate -->
  <div class="panel-body">
    <!-- New Class Form -->
    {!! Form::model($classroom, [
        'route' => ['classroom.store'],
        'class' => 'form-horizontal'
    ]) !!}

      <!-- Class Code -->
      <div class="form-group row">
        {!! Form::label('class_code', 'Class Code', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('class_code', null, [
              'id' => 'class_code',
              'class' => 'form-control',
              'maxlength' => 4,
          ]) !!}
        </div>
      </div>

      <!-- Class Name -->
      <div class="form-group row">
        {!! Form::label('class_name', 'Class Name', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('name', null, [
              'id' => 'name',
              'class' => 'form-control',
              'maxlength' => 5,
          ]) !!}
        </div>
      </div>

      <!-- Class Description -->
      <div class="form-group row">
        {!! Form::label('class_description', 'Class Description', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::textarea('description', null, [
              'id' => 'description',
              'class' => 'form-control'
          ]) !!}
        </div>
      </div>

      <!-- Class Room -->
      <div class="form-group row">
        {!! Form::label('class_room', 'Class Room', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::select('room', Common::$rooms, null, [
              'class' => 'form-control',
              'placeholder' => '- Select Room -'
          ]) !!}
        </div>
      </div>

      <!-- Class Teacher -->
      <div class="form-group row">
        {!! Form::label('class_teacher_id', 'Teacher In Charge', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::select('teacher_id',
              Teacher::pluck('name', 'id'), null, [
                'class' => 'form-control',
                'placeholder' => '- Select Teacher -'
          ]) !!}
        </div>
      </div>

      <!-- Submit Button -->
      <div class="form-group row">
        <div class="col-sm-offset-3 col-sm-6">
          {!! Form::button('Save', [
            'type' => 'submit',
            'class' => 'btn btn-primary'
          ]) !!}
        </div>
      </div>
      {!! Form::close() !!}
  </div>
@endsection
