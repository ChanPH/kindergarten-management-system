<?php
  use App\Common;
  use App\Teacher;
?>
@extends('layouts.app')
@section('content')
  <!-- Bootstrap Boilerplate -->
  <div class="panel-body">
    <table class="table table-striped task-table">
      <!-- Table Headings -->
      <thead>
        <tr>
          <th>Attribute</th>
          <th>Value</th>
        </tr>
      </thead>
      <!-- Table Body -->
      <tbody>
        <tr>
          <td>Class Code</td>
          <td>{{ $classroom->class_code }}</td>
        </tr>
        <tr>
          <td>Class Name</td>
          <td>{{ $classroom->name }}</td>
        </tr>
        <tr>
          <td>Class Description</td>
          <td>{!! nl2br($classroom->description) !!}</td>
        </tr>
        <tr>
          <td>Class Room</td>
          <td>{{ Common::$rooms[$classroom->room] }}</td>
        </tr>
        <tr>
          <td>Teacher In Charge</td>
          <td>{{ Teacher::value('name')}}</td>
        </tr>
      </tbody>
    </table>
  </div>

@endsection
