<?php
  use App\Common;
  use App\Teacher;
?>
@extends('layouts.app')
@section('content')

  <!-- Bootstrap Boilerplate -->
  <div class="panel-body">
    @if (count($classroom) >0 )
      <table class="table table-striped task-table">
        <!-- Table Headings -->
        <thead>
          <tr>
            <th>No.</th>
            <th>Class Code</th>
            <th>Class Name</th>
            <th>Class Description</th>
            <th>Class Room</th>
            <th>Teacher In Charge</th>
            <th>Created At</th>
            <th>Updated At</th>
          </tr>
        </thead>

        <!-- Table Body -->
        <tbody>
          @foreach($classroom as $i => $classroom)
            <tr>
              <td class="table-text">
                <div>{{ $i+1 }}</div>
              </td>
              <td class="table-text">
                <div>{!! link_to_route(
                          'classroom.show',
                          $title = $classroom->class_code,
                          $parameters = [
                            'id' => $classroom->id,
                          ]
                  ) !!}
                </div>
              </td>
              <td class="table-text">
                <div>{{ $classroom->name }}</div>
              </td>
              <td class="table-text">
                <div>{{ $classroom->description }}</div>
              </td>
              <td class="table-text">
                <div>{{ Common::$rooms[$classroom->room] }}</div>
              </td>
              <td class="table-text">
                <div>{!! link_to_route(
                          'teacher.show',
                          $title = $classroom->teacher->name,
                          $parameters = [
                              'id' => $classroom->teacher_id,
                          ]
                      ) !!}
                </div>
              </td>
              <td class="table-text">
                <div>{{ $classroom->created_at }}</div>
              </td>
              <td class="table-text">
                <div>{{ $classroom->updated_at }}</div>
              </td>
              <td class="table-text">
                <div>{!! link_to_route(
                          'classroom.edit',
                          $title = 'Edit',
                          $parameters = [
                              'id' => $classroom->id,
                          ]
                  ) !!}
                </div>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      @else
        <div>
          <tbody>
            <h1>No records found</h1>
          </tbody>
        </div>
      @endif

      <!-- Create New Classroom button -->
      <div class="form-group row">
        <div class="col-sm-offset-3 col-sm-6">
          <a href="{{ route('classroom.create') }}" type='Button' class='btn btn-primary'>Create New Classroom</a>
        </div>
      </div>

    </div>
@endsection
