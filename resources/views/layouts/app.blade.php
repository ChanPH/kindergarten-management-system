<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Kindergarten Management System</title>
    <!-- CSS And JavaScript -->
    <!-- CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- Javascript -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" rel="stylesheet">
    <!-- Javascript Bundle -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <nav class="navbar navbar-default">
        <!-- Navbar Contents -->
      </nav>
    </div>

    @yield('content')
  </body>
</html>
