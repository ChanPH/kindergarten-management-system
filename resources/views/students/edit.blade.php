<?php
  use App\Common;
  use App\Student;
  use App\Classroom;
?>
@extends('layouts.app')
@section('content')

@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

  <!-- Bootstrap Boilerplate -->
    <!-- Existing Student Form -->
    {!! Form::model($student, [
        'route' => [ 'student.update', $student->id ],
        'method' => 'put',
        'class' => 'form-horizontal'
    ]) !!}
      <!-- Student ID -->
      <div class="form-group row">
        {!! Form::label('student_id', 'Student ID', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('student_id', $student->student_id, [
              'id' => 'student_id',
              'class' => 'form-control',
              'maxlength' => 5
          ]) !!}
        </div>
      </div>

      <!-- NRIC -->
      <div class="form-group row">
        {!! Form::label('student_nric', 'Student NRIC', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('nric', $student->nric, [
              'id' => 'nric',
              'class' => 'form-control',
              'maxlength' => 12
          ]) !!}
        </div>
      </div>

      <!-- Name -->
      <div class="form-group row">
        {!! Form::label('student_name', 'Student Name', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('name', $student->name, [
              'id' => 'name',
              'class' => 'form-control',
              'maxlength' => 100
          ]) !!}
        </div>
      </div>

      <!-- Gender -->
      <div class="form-group row">
        {!! Form::label('student_gender', 'Student Gender', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          @foreach(Common::$genders as $key => $val)
              {!! Form::radio('gender', $key) !!} {{$val}}
          @endforeach
        </div>
      </div>

      <!-- Address -->
      <div class="form-group row">
        {!! Form::label('student_address', 'Student Address', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::textarea('address', $student->address, [
              'id' => 'address',
              'class' => 'form-control'
          ]) !!}
        </div>
      </div>

      <!-- Postcode -->
      <div class="form-group row">
        {!! Form::label('student_postcode', 'Postcode', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('postcode', $student->postcode, [
              'id' => 'postcode',
              'class' => 'form-control',
              'maxlength' => 5
          ]) !!}
        </div>
      </div>

      <!-- City -->
      <div class="form-group row">
        {!! Form::label('student_city', 'City', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('city', $student->city, [
              'id' => 'city',
              'class' => 'form-control',
              'maxlength' => 50
          ]) !!}
        </div>
      </div>

      <!-- state -->
      <div class="form-group row">
        {!! Form::label('student_state', 'State', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::select('state', Common::$states, $student->state, [
              'class' => 'form-control',
              'placeholder' => '- Select State -'
          ]) !!}
        </div>
      </div>

      <!-- guardian -->
      <div class="form-group row">
        {!! Form::label('student_guardian', 'Guardian Name', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('guardian', $student -> guardian, [
              'id' => 'guardian',
              'class' => 'form-control',
              'maxlength' => 100
          ]) !!}
        </div>
      </div>

      <!-- guardian phone no -->
      <div class="form-group row">
        {!! Form::label('student_phone_no', 'Guardian Phone No', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('guardian_phone_no', $student -> guardian_phone_no, [
              'id' => 'guardian_phone_no',
              'class' => 'form-control',
              'maxlength' => 10,
          ]) !!}
        </div>
      </div>

      <!-- Subject taken by students -->
      <div class="form-group row">
        {!! Form::label('subject_taken_by_student', 'Subject Taken', [
            'class' => 'control-label col-sm-3',
        ]) !!}
        <div class="col-sm-9">
          @foreach(Classroom::pluck('name', 'id') as $key => $val)
            {!! Form::checkbox('classroom_id[]', $key) !!}
            {{ $val }}
          @endforeach
        </div>
      </div>

      <!-- submit -->
      <div class="form-group row">
        <div class="col-sm-offset-3 col-sm-6">
          {!! Form::button('Update', [
              'type' => 'submit',
              'class' => 'btn btn-primary'
          ]) !!}
        </div>
      </div>

    {!! Form::close() !!}
  </div>

@endsection
