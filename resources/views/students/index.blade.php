<?php
  use App\Common;
  use App\Classroom;
?>
@extends('layouts.app')
@section('content')

  <!-- Bootstrap Boilerplate -->
  <div class="panel-body">
    @if (count($students) > 0)
      <table class="table table-striped task-table">
        <!-- Table Headings -->
        <thead>
          <tr>
            <th>No.</th>
            <th>Student ID</th>
            <th>NRIC</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Address</th>
            <th>Postcode</th>
            <th>City</th>
            <th>State</th>
            <th>Guardian</th>
            <th>Guardian Phone No</th>
            <th>Classes Taken</th>
            <th>Created At</th>
            <th>Updated At</th>
          </th>
        </thead>

        <!-- Table Body -->
        <tbody>
          @foreach ($students as $i => $student)
            <tr>
              <td class="table-text">
                <div>{{ $i+1 }}</div>
              </td>
              <td class="table-text">
                <div>{!! link_to_route(
                            'student.show',
                            $title = $student->student_id,
                            $parameters = [
                              'id' => $student->id,
                            ]
                     ) !!}
                </div>
              </td>
              <td class="table-text">
                <div>{{ $student->nric }}</div>
              </td>
              <td class="table-text">
                <div>{{ $student->name }}</div>
              </td>
              <td class="table-text">
                <div>{{ Common::$genders[$student->gender] }}</div>
              </td>
              <td class="table-text">
                <div>{{ $student->address }}</div>
              </td>
              <td class="table-text">
                <div>{{ $student->postcode }}</div>
              </td>
              <td class="table-text">
                <div>{{ $student->city }}</div>
              </td>
              <td class="table-text">
                <div>{{ Common::$states[$student->state] }}</div>
              </td>
              <td class="table-text">
                <div>{{ $student->guardian }}</div>
              </td>
              <td class="table-text">
                <div>{{ $student->guardian_phone_no }}</div>
              </td>
              <td class="table-text">
                @foreach($student->classrooms as $classroom)
                    <div>{{ $classroom->pivot->classroom_id }}</div>
                @endforeach
              </td>
              <td class="table-text">
                <div>{{ $student->created_at }}</div>
              </td>
              <td class="table-text">
                <div>{{ $student->updated_at }}</div>
              </td>
              <td class="table-text">
                <div>{!! link_to_route(
                            'student.edit',
                            $title = 'Edit',
                            $parameters = [
                                'id' => $student->id,
                            ]
                     ) !!}
                </div>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
    @else
        <div>
          <tbody>
            <h1>No records found</h1>
          </tbody>
        </div>
    @endif

    <!-- Create New Student button -->
    <div class="form-group row">
      <div class="col-sm-offset-3 col-sm-6">
        <a href="{{ route('student.create') }}" type='Button' class='btn btn-primary'>Insert New Student</a>
      </div>
    </div>

  </div>
@endsection
