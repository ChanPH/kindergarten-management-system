<?php
  use App\Common;
  use App\Classroom;
?>
@extends('layouts.app')
@section('content')

@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

  <!-- Bootstrap Boilerplate -->
  <div class="panel-body">
    <!-- New Student Form -->
    {!! Form::model($student, [
        'route' => ['student.store'],
        'class' => 'form-horizontal',
    ]) !!}

      <!-- Student ID -->
      <div class="form-group row">
        {!! Form::label('student_id', 'Student ID', [
          'class' => 'control-label col-sm-3',
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('student_id', null, [
              'id' => 'student_id',
              'class' => 'form-control',
              'maxlength' => 5,
          ]) !!}
        </div>
      </div>

      <!-- Student NRIC -->
      <div class="form-group row">
        {!! Form::label('student_nric', 'Student NRIC', [
          'class' => 'control-label col-sm-3',
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('nric', null, [
              'id' => 'nric',
              'class' => 'form-control',
              'maxlength'=> 12,
          ]) !!}
        </div>
      </div>

      <!-- Student Name -->
      <div class="form-group row">
        {!! Form::label('student_name', 'Student Name', [
          'class' => 'control-label col-sm-3',
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('name', null, [
              'id' => 'name',
              'class' => 'form-control',
              'maxlength' => 100,
          ]) !!}
        </div>
      </div>

      <!-- Student Gender -->
      <div class="form-group row">
        {!! Form::label('student_gender', 'Gender', [
            'class' => 'control-label col-sm-3',
        ]) !!}
        <div class="col-sm-9">
          @foreach(Common::$genders as $key => $val)
            {!! Form::radio('gender', $key) !!} {{$val}}
          @endforeach
        </div>
      </div>

      <!-- Student Address -->
      <div class="form-group row">
        {!! Form::label('student_address', 'Address', [
            'class' => 'control-label col-sm-3',
        ]) !!}
        <div class="col-sm-9">
          {!! Form::textarea('address', null, [
              'id' => 'address',
              'class' => 'form-control',
          ]) !!}
        </div>
      </div>

      <!-- Student Postcode -->
      <div class="form-group row">
        {!! Form::label('student_postcode', 'Postcode', [
          'class' => 'control-label col-sm-3',
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('postcode', null, [
              'id' => 'postcode',
              'class' => 'form-control',
              'maxlength' => 5,
          ]) !!}
        </div>
      </div>

      <!-- Student City -->
      <div class="form-group row">
        {!! Form::label('student_city', 'City', [
            'class' => 'control-label col-sm-3',
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('city', null, [
              'id' => 'city',
              'class' => 'form-control',
              'maxlength' => 50,
          ]) !!}
        </div>
      </div>

      <!-- Student State -->
      <div class="form-group row">
        {!! Form::label('student_state', 'State', [
            'class' => 'control-label col-sm-3',
        ]) !!}
        <div class="col-sm-9">
          {!! Form::select('state', Common::$states, null, [
              'class' => 'form-control',
              'placeholder' => '- Select State -',
          ]) !!}
        </div>
      </div>

      <!-- Student Guardian -->
      <div class="form-group row">
        {!! Form::label('student_guardian', 'Guardian', [
            'class' => 'control-label col-sm-3',
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('guardian', null, [
              'id' => 'guardian',
              'class' => 'form-control',
              'maxlength' => 100,
          ]) !!}
        </div>
      </div>

      <!-- Student Guardian Phone No -->
      <div class="form-group row">
        {!! Form::label('student_guardian_phone_no', 'Guardian Phone No', [
            'class' => 'control-label col-sm-3',
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('guardian_phone_no', null, [
              'id' => 'guaridan_phone_no',
              'class' => 'form-control',
              'maxlength' => 10.
          ]) !!}
        </div>
      </div>

      <!-- Subject taken by students -->
      <div class="form-group row">
        {!! Form::label('subject_taken_by_student', 'Subject Taken', [
            'class' => 'control-label col-sm-3',
        ]) !!}
        <div class="col-sm-9">
          @foreach(Classroom::pluck('name', 'id') as $key => $val)
            {!! Form::checkbox('classroom_id[]', $key) !!}
            {{ $val }}
          @endforeach
        </div>
      </div>

      <!-- Submit Button -->
      <div class="form-group row">
        <div class="col-sm-offset-3 col-sm-6">
          {!! Form::button('Save', [
              'type' => 'submit',
              'class' => 'btn btn-primary',
          ]) !!}
        </div>
      </div>
    {!! Form::close() !!}
  </div>
@endsection
