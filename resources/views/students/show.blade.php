<?php
    use App\Common;
?>
@extends('layouts.app')
@section('content')

  <!-- Bootstrap Boilerplate -->
  <div class="panel-body">
    <table class="table table-striped task-table">
      <!-- Table Headings -->
      <thead>
        <tr>
          <th>Attribute</th>
          <th>Value</th>
        </tr>
      </thead>
      <!-- Table Body -->
      <tbody>
        <tr>
          <td>Student ID</td>
          <td>{{ $student->student_id }}</td>
        </tr>
        <tr>
          <td>Student NRIC</td>
          <td>{{ $student->nric }}</td>
        </tr>
        <tr>
          <td>Student Name</td>
          <td>{{ $student->name }}</td>
        </tr>
        <tr>
          <td>Student Gender</td>
          <td>{{ Common::$genders[$student->gender] }}</td>
        </tr>
        <tr>
          <td>Student Address</td>
          <td>{!!  nl2br($student->address) !!}</td>
        </tr>
        <tr>
          <td>Postcode</td>
          <td>{{ $student->postcode }}</td>
        </tr>
        <tr>
          <td>City</td>
          <td>{{ $student->city }}</td>
        </tr>
        <tr>
          <td>State</td>
          <td>{{ Common::$states[$student->state] }}</td>
        </tr>
        <tr>
          <td>Guardian Name</td>
          <td>{{ $student->guardian }}</td>
        </tr>
        <tr>
          <td>Guardian Phone No</td>
          <td>{{ $student->id }}</td>
        </tr>
        <tr>
          <td>Classes Taken</td>
          @foreach($student->classrooms as $classroom)
              <td>{{ $classroom->pivot->classroom_id }}</td>
          @endforeach
        </tr>
      </tbody>
    </table>
  </div>

@endsection
