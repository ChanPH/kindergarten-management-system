<?php
  use App\Common;
?>
@extends('layouts.app')
@section('content')

  <!-- Bootstrap Boilerplate -->
  <div class="panel-body">
    <table class="table table-striped task-table">
      <!-- Table Headings -->
      <thead>
        <tr>
          <th>Attribute</th>
          <th>Value</th>
        </tr>
      </thead>
      <!-- Table Body -->
      <tbody>
        <tr>
          <td>Teacher ID</td>
          <td>{{ $teacher->teacher_id }}</td>
        </tr>
        <tr>
          <td>Teacher NRIC</td>
          <td>{{ $teacher->nric }}</td>
        </tr>
        <tr>
          <td>Teacher Name</td>
          <td>{{ $teacher->name }}</td>
        </tr>
        <tr>
          <td>Teacher Gender</td>
          <td>{{ Common::$genders[$teacher->gender] }}</td>
        </tr>
        <tr>
          <td>Teacher Phone No</td>
          <td>{{ $teacher->phone_no }}</td>
        </tr>
        <tr>
          <td>Teacher Qualification</td>
          <td>{{ Common::$qualifications[$teacher->qualification] }}</td>
        </tr>
      </tbody>
    </table>
  </div>
@endsection
