<?php
  use App\Common;
?>
@extends('layouts.app')
@section('content')

@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

  <!-- Bootstrap Boilerplate -->
    <!-- Existing Teacher Form -->
    {!! Form::model($teacher, [
        'route' => ['teacher.update', $teacher->id],
        'method' => 'put',
        'class' => 'form-horizontal'
    ]) !!}
      <!-- Teacher ID -->
      <div class="form-group row">
        {!! Form::label('teacher_id', 'Teacher ID', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('teacher_id', $teacher->teacher_id, [
              'id' => 'teacher_id',
              'class' => 'form-control',
              'maxlength' => 4
          ]) !!}
        </div>
      </div>

      <!-- Teacher NRIC -->
      <div class="form-group row">
        {!! Form::label('teacher_nric', 'Teacher NRIC', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('nric', $teacher->nric, [
              'id' => 'nric',
              'class' => 'form-control',
              'maxlength' => 12
          ]) !!}
        </div>
      </div>

      <!-- Teacher Name -->
      <div class="form-group row">
        {!! Form::label('teacher_name', 'Teacher Name', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('name', $teacher->name, [
              'id' => 'name',
              'class' => 'form-control',
              'maxlength' => 100
          ]) !!}
        </div>
      </div>

      <!-- Teacher Gender -->
      <div class="form-group row">
        {!! Form::label('teacher_gender', 'Teacher Gender', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          @foreach(Common::$genders as $key => $val)
            {!! Form::radio('gender', $key) !!} {{$val}}
          @endforeach
        </div>
      </div>

      <!-- Teacher Phone No -->
      <div class="form-group row">
        {!! Form::label('teacher_phone_no', 'Teacher Phone No', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::text('phone_no', $teacher->phone_no, [
              'id' => 'phone_no',
              'class' => 'form-control',
              'maxlength' => 10
          ]) !!}
        </div>
      </div>

      <!-- Teacher Qualification -->
      <div class="form-group row">
        {!! Form::label('teacher_qualification', 'Teacher Qualification', [
            'class' => 'control-label col-sm-3'
        ]) !!}
        <div class="col-sm-9">
          {!! Form::select('qualification', Common::$qualifications, $teacher->qualification, [
              'class' => 'form-control',
              'placeholder' => '- Select Qualification -'
          ]) !!}
        </div>
      </div>

      <!-- Submit -->
      <div class="form-group row">
        <div class="col-sm-offset-3 col-sm-6">
          {!! Form::button('Update', [
              'type' => 'submit',
              'class' => 'btn btn-primary'
          ]) !!}
        </div>
      </div>

      {!! Form::close() !!}
    </div>
@endsection
