<?php
  use App\Common;
?>
@extends('layouts.app')
@section('content')

  <!-- Bootstrap Boilerplate -->
  <div class="panel-body">
    @if (count($teachers) >0)
    <table class="table table-striped task-table">
      <!-- Table Headings -->
      <thead>
        <tr>
          <th>No.</th>
          <th>Teacher ID</th>
          <th>Teacher NRIC</th>
          <th>Teacher Name</th>
          <th>Teacher Gender</th>
          <th>Teacher Phone No</th>
          <th>Teacher Qualification</th>
          <th>Created At</th>
          <th>Updated At</th>
        </tr>
      </thead>
      <!-- Table Body -->
      <tbody>
        @foreach($teachers as $i => $teacher)
        <tr>
          <td class="table-text">
            <div>{{ $i+1 }}</div>
          </td>
          <td class="table-text">
            <div>{!! link_to_route(
                    'teacher.show',
                    $title = $teacher->teacher_id,
                    $parameters = [
                      'id' => $teacher->id,
                    ]
              ) !!}
            </div>
          </td>
          <td class="table-text">
            <div>{{ $teacher->nric }}</div>
          </td>
          <td class="table-text">
            <div>{{ $teacher->name }}</div>
          </td>
          <td class="table-text">
            <div>{{ Common::$genders[$teacher->gender] }}</div>
          </td>
          <td class="table-text">
            <div>{{ $teacher->phone_no }}</div>
          </td>
          <td class="table-text">
            <div>{{ Common::$qualifications[$teacher->qualification] }}</div>
          </td>
          <td class="table-text">
            <div>{{ $teacher->created_at }}</div>
          </td>
          <td class="table-text">
            <div>{{ $teacher->updated_at }}</div>
          </td>
          <td class="table-text">
            <div>{!! link_to_route(
                      'teacher.edit',
                      $title = 'Edit',
                      $parameters = [
                          'id' => $teacher->id,
                      ]
              ) !!}
            </div>
          </td>
        </tr>
      @endforeach
      </tbody>
    </table>
  @else
    <div>
      <tbody>
        <h1>No records found</h1>
      </tbody>
    </div>
  @endif

  <!-- Create New Classroom button -->
  <div class="form-group row">
    <div class="col-sm-offset-3 col-sm-6">
      <a href="{{ route('teacher.create') }}" type='Button' class='btn btn-primary'>Insert New Teacher</a>
    </div>
  </div>

  </div>
@endsection
