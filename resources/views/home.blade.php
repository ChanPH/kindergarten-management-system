@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <h2>{{ session('status') }}</h2>
                    @endif

                    You are logged in!

                    <div>
                      <!-- Goto Classroom button -->
                      <a href="{{ route('classroom.index') }}" type='Button' class='btn btn-primary'>Classroom Overview</a>
                      <!-- Goto Students button -->
                      <a href="{{ route('student.index') }}" type='Button' class='btn btn-primary'>Student Overview</a>
                      <!-- Goto Teachers button -->
                      <a href="{{ route('teacher.index') }}" type='Button' class='btn btn-primary'>Teacher Overview</a>
                    </div>

                    <div>
                      <article>
</br><pre>
  This is a Kindergarten Management System designed
  meant for the internal usage by the management personnel
  inside the Kindergarten.  The users of the system
  primary consists of the following:
  (1) Program Handler        => administrator
  (2) Kindergarten Teachers  => users
  (3) Kindergarten Kids      => users
</pre></br>
                      </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
